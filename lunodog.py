#!/usr/bin/python3
# encoding: utf-8
import time, asyncio, os

#my modules
from modules import console, config, bot, client, database
console.init()
bot.init()
database.init()
config.init()
client.init()


async def bot_run():
	while console.alive:
		frametime = time.time()
		bot.run(frametime)
		console.run()
		await client.send() #send messages
		await asyncio.sleep(0.5) # task runs every 0.5 seconds
	#quit gracefully
	try:
		await client.close()
	except:
		pass
	console.log.close()
	print("QUIT NOW.")
	os._exit(0)

client.c.loop.create_task(bot_run())
client.run() #runs until ctrl+c
