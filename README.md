# Lunodog
A discord bot for cpma community.

#Requirements
Python 3.5+, sqlite3 module for python, GeoIP, discord.py v0.10+.

# License
Copyright (C) 2018 Leshaka.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See 'GNU GPLv3.txt' for GNU General Public License.
