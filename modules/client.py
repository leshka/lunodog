#!/usr/bin/python3
# encoding: utf-8
import discord, traceback, time
from modules import console, config, bot

def init():
	global ready, send_queue, silent
	ready = False
	silent = False
	send_queue = []

def process_connection():
	console.display('SYSTEM| Logged in as: {0}, ID: {1}'.format(c.user.name, c.user.id))

async def send(): #send messages in queue
	if len(send_queue) > 0:
		data = send_queue.pop(0)
		if data[0] == 'msg':
			print("send msg event!")
			destination, content = data[1], data[2]
			console.display('SEND| /{0}: {1}'.format(destination, content))
			if not silent:
				print("sending the message!")
				try:
					await destination.send(content)
				except:
					console.display("ERROR| Could not send the message. "+str(traceback.format_exc()))
		elif data[0] == 'embed':
			if not silent:
				try:
					await data[1].send(embed=data[2])
				except:
					console.display("ERROR| Failed to send embed with '{0}'. {1}".format(str(data[2].to_dict()),traceback.format_exc()))
		elif data[0] == 'leave_server':
			for serv in c.guilds:
				if serv.id == data[1]:
					console.display("Leaving {0}...".format(serv.name))
					await c.leave_guild(serv)
					break

async def close(): #on quit
	if True:#c.is_logged_in:
		try:
			await c.logout()
			print("Successfully logged out.")
		except Exception as e:
			print("Error on logging out. {0}".format(str(e)))
	else:
		print("Connection is allready closed.")

### api for bot.py ###
def find_role_by_name(channel, name):
	name = name.lower()
	for role in channel.guild.roles:
		if name == role.name.lower():
			return role
	return None

def notice_embed(channel, embed):
	send_queue.append(['embed', channel, embed])

async def edit_role(**fields):
	await c.edit_role(**fields)

async def remove_roles(member, *roles):
	await c.remove_roles(member, *roles)

async def add_roles(member, *roles):
	await c.add_roles(member,*roles)

async def send_message(dest, msg): #send msg asap, dont put it in queue
	await dest.send(msg)

def notice(channel, msg):
	send_queue.append(['msg', channel, msg])

def reply(channel, member, msg):
	send_queue.append(['msg', channel, "<@{0}>, {1}".format(member.id, msg)])
	
def private_reply(member, msg):
	send_queue.append(['msg', member, msg])
	
def get_member_by_nick(channel, nick):
	server = c.get_guild(channel.guild.id)
	nick = nick.lower()
	return discord.utils.find(lambda m: m.name.lower() == nick or (m.nick and m.nick.lower() == nick), server.members)

def get_member_by_id(channel, highlight):
	server = c.get_guild(channel.guild.id)
	memberid = highlight.lstrip('<@!').rstrip('>')
	return discord.utils.find(lambda m: m.id == memberid, server.members)

### discord events ###
c = discord.Client()

@c.event
async def on_ready():
	global ready
	if not ready:
		process_connection()
		ready = True
	else:
		console.display("DEBUG| Unexpected on_ready event!")

@c.event
async def on_message(message):
	try:
		await bot.processmsg(message)
	except:
		console.display("ERROR| Error processing message: {0}".format(traceback.format_exc()))

@c.event
async def on_member_update(before, after):
	bot.update_member(before, after)

@c.event
async def on_member_join(member):
	bot.on_member_join(member)

### connect to discord ###
def run():
	while True:
		try:
			if config.cfg.DISCORD_TOKEN != "":
				console.display("SYSTEM| logging in with token...")
				c.loop.run_until_complete(c.start(config.cfg.DISCORD_TOKEN))
			else:
				console.display("SYSTEM| logging in with username and password...")
				c.loop.run_until_complete(c.start(config.cfg.USERNAME, config.cfg.PASSWORD))
			c.loop.run_until_complete(c.connect())
		except KeyboardInterrupt:
			console.display("ERROR| Keyboard interrupt.")
			console.terminate()
			c.loop.run_until_complete(close())
			print("QUIT NOW.")
			break
		except Exception as e:
			console.display("ERROR| Disconnected from the server: "+str(e)+"\nReconnecting in 15 seconds...")
			break
			time.sleep(15)
