#!/usr/bin/python3
import sqlite3, operator
from datetime import timedelta
from time import time
import re

from modules import console
#INIT

def init():
	global conn, c
	conn = sqlite3.connect("database.sqlite3")
	c = conn.cursor()
	check_tables()

def update_users(users):
	console.display("DATABASE| Updating users db...")
	now = time()
	for user in users:

		c.execute("INSERT OR IGNORE INTO users (id, name) VALUES (?, ?)", (user.id, user.name))
		if user.nick:
			c.execute("INSERT OR IGNORE INTO user_nicks (user_id, nick, changed_at) VALUES (?, ?, ?)", (user.id, user.nick, now))

	conn.commit()
	console.display("DATABASE| Done.")

def add_user_nick(user_id, user_nick):
	now = int(time())
	c.execute("INSERT OR REPLACE INTO user_nicks (user_id, nick, changed_at) VALUES (?, ?, ?)", (user_id, user_nick, now))
	conn.commit()

def update_user(user):
	now = int(time())
	c.execute("INSERT OR REPLACE INTO users (id, name, last_seen) VALUES (?, ?, ?)", (user.id, user.name, now))
	conn.commit()
	
def last_seen(nick):
	c.execute("SELECT last_seen FROM users WHERE name = ? COLLATE NOCASE", (nick, ))
	l = c.fetchone()
	if not l:
		c.execute("SELECT user_id FROM user_nicks WHERE nick = ? COLLATE NOCASE", (nick, ))
		x = c.fetchone()
		if not x:
			return None
		c.execute("SELECT last_seen FROM users WHERE id = ?", (x[0], ))
		l = c.fetchone()
		if not l:
			return None
	return l[0]

def last_seen_id(targetid):
	c.execute("SELECT last_seen FROM users WHERE id = ?", (targetid, ))
	l = c.fetchone()
	if not l:
		return None
	return l[0]

def get_nicks(nick):
	c.execute("SELECT id FROM users WHERE name = ? COLLATE NOCASE", (nick, ))
	l = c.fetchone()
	if not l:
		c.execute("SELECT user_id FROM user_nicks WHERE nick = ? COLLATE NOCASE", (nick, ))
		l = c.fetchone()
		if not l:
			return None
	c.execute("SELECT nick FROM user_nicks WHERE user_id = ?", (l[0], ))
	l = c.fetchall()
	if l != []:
		return [i[0] for i in l]
	else:
		return None

def check_greetings(user_id, server_id):
	c.execute("SELECT user_id FROM greetings WHERE user_id = ? AND server_id = ?", (user_id, server_id))
	l = c.fetchone()
	if l:
		return True
	else:
		return False

def add_greeting(user_id, server_id):
	c.execute("INSERT OR REPLACE INTO greetings (user_id, server_id) VALUES (?, ?)", (user_id, server_id))
	conn.commit()

def check_tables():
	c.execute("SELECT name FROM sqlite_master WHERE type='table'")
	tables = c.fetchall()
	tables = list(map(lambda x: x[0], tables))

	if 'users' not in tables:
		c.execute("""CREATE TABLE "users" ( `id` TEXT, `name` TEXT, `last_seen` INTEGER, PRIMARY KEY(`id`) )""")
	if 'user_nicks' not in tables:
		c.execute("""CREATE TABLE `user_nicks` ( `user_id` TEXT, `nick` TEXT, `changed_at` INTEGER, PRIMARY KEY(`user_id`,`nick`) )""")
	if 'greetings' not in tables:
		c.execute("""CREATE TABLE `greetings` ( `user_id` TEXT, `server_id` TEXT )""")

	conn.commit()

def close():
	conn.commit()
	conn.close()
