#!/usr/bin/python3
# encoding: utf-8
import traceback, time, datetime, re, GeoIP, asyncio, requests
import xml.etree.ElementTree as etree
import urllib.parse
from discord import Embed
from random import randint

from modules import client, config, console, database

def init():
	global last_update, emoji_re, gizma_url, geoip, qstat_lock, qstat_messages
	geoip = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
	last_update = []
	emoji_re = re.compile("^<:(.+):\d*>$")
	gizma_url = "https://cdn.discordapp.com/emojis/418830717271670784.png"
	qstat_lock = False
	qstat_messages = []

def on_connect():
	database.update_users(client.c.get_all_members())

async def processmsg(message):
	#do not process pictures etc
	if len(message.embeds) or len(message.attachments):
		return

	if message.author.id == client.c.user.id and message.content == "<:cpma:418079122795790367> ...listing servers... <:cpma:418079122795790367>":
		console.display("DEBUG| qstat message catched with id {0}!".format(message.id))
		qstat_messages.append(message)

	if message.content[0] != config.cfg.PREFIX or message.author.bot:
		return

	console.display("{0}>{1}: {2}".format(message.channel, message.author, message.content))
	msg = message.content.lstrip(config.cfg.PREFIX)
	msgtup = msg.split(" ")
	lowertup = msg.lower().split(" ")

	if lowertup[0] == "qstat":
		await qstat(message.channel, lowertup[1:])

	elif lowertup[0] == "seen":
		last_seen(message.channel, message.author, msg.lstrip("seen").lstrip(" ").lower())

	elif lowertup[0] == "nicks":
		get_nicks(message.channel, message.author, msg.lstrip("nicks").lstrip(" ").lower())

	elif lowertup[0] == "emoji_list":
		get_emoji_list(message.channel, message.author)

	elif lowertup[0] == "emoji":
		get_emoji(message.channel, message.author, lowertup[1:])

	elif lowertup[0] == "anime":
		post_anime(message.channel, message.author, lowertup[1:])

	#elif lowertup[0] == "gif":
	#	get_emoji(message.channel, message.author, lowertup[1:], gif=True)

	elif lowertup[0] == "gizmaquote":
		gizma_quote(message.channel, message.author, msg.lstrip("gizmaquote").lstrip(" "))

	elif lowertup[0] == "avatar":
		get_avatar(message.channel, message.author, lowertup[1:])

	elif lowertup[0] == "commands":
		client.notice(message.channel, config.cfg.COMMANDS)

def update_member(before, after): #on status change
	global last_update
	#supress dublicates (same updates from multiple channels)
	update = [after.nick, after.name, after.status]
	if  update == last_update:
		return
	last_update = update
	
	if str(after.status) != str(before.status) and str(after.status) == "offline":
		#console.display("DEBUG| update seen event on {0}...".format(after.name))
		database.update_user(after)

	if before.name != after.name:
		console.display("DEBUG| update name event on {0}...".format(after.name))
		database.update_user(after)
		database.add_user_nick(after.id, before.name)
		
	if after.nick != before.nick and after.nick != None:
		#console.display("DEBUG| update nick event on {0}...".format(after.name))
		database.add_user_nick(after.id, after.nick)


def run(frametime):
	pass

#commands

def _parse_q3server_xml(server): #parse and return if its populated
	try:
		hostname = server.find("hostname").text
		name = server.find("name").text
		name = re.sub('discord\.(me|gg)', 'дискорд.жж', name)
		mapname = server.find("map").text
		mode = server.find("rules/rule[@name='mode_current']").text
		private = server.find("rules/rule[@name='g_needpass']").text
		players = ", ".join([p.text for p in server.findall("players/player/name")])
		if len(players):
			if players.lower().find("gizma") >= 0:
				icon = ":swimmer:"
			elif private == "1":
				icon = ":lock:"
			else:
				icon = "<:cpma:418079122795790367>" 

			flag = geoip.country_code_by_addr(re.sub(r":[0-9]+", "", hostname))
			if flag:
				flag = ":flag_{0}:".format(flag.lower())
			else:
				flag = ":grey_question:"

			return([icon, flag, mode, mapname, hostname, name, players])
		else:
			return(None)
	except:
		return(None)

async def q3_servers():
	servers = []
	known_servers = config.cfg.Q3_SERVERS_BLACKLIST.split(" ")
	for msrv in config.cfg.Q3_MASTERSERVERS.split(" "):
		process = await asyncio.create_subprocess_shell("quakestat -R -P -xml -q3m {0}".format(msrv), stdout=asyncio.subprocess.PIPE, loop=client.c.loop)
		stdout, stderr = await process.communicate()
		xml = etree.fromstring(stdout.decode("utf-8"))
		for server in xml:
			addr = server.get("address")
			if addr not in known_servers:
				known_servers.append(addr)
				l = _parse_q3server_xml(server)
				if l:
					servers.append(l)

	for srv in config.cfg.Q3_SERVERS.split(" "):
		if srv not in known_servers:
			process = await asyncio.create_subprocess_shell("quakestat -R -P -xml -q3s {0}".format(srv), stdout=asyncio.subprocess.PIPE)
			stdout, stderr = await process.communicate()
			xml = etree.fromstring(stdout.decode("utf-8"))
			l = _parse_q3server_xml(xml[0])
			if l: servers.append(l)
		else:
			console.display("DEBUG| qstat: scipping server {0}".format(srv))

	if len(servers):
		return "\n".join(["{0}{1} [**{2}**] {3} `/connect {4}` | `{5}` : {6}".format(*i) for i in servers])
	else:
		return "ded gaem... :skull_crossbones:"

async def qstat(channel, args):
	global qstat_lock, qstat_messages
	await channel.send("<:cpma:418079122795790367> ...listing servers... <:cpma:418079122795790367>")
	if not qstat_lock:
		qstat_lock = True
		reply = await q3_servers()
		for msg in qstat_messages:
			console.display("DEBUG| Editing message "+str(msg.id))
			await msg.edit(content=reply)
		qstat_messages = []
		qstat_lock = False

def last_seen(channel, user, msg):
	if not len(msg):
		client.reply(channel, user, "You must specify a nick name.")
		return

	online = False

	if re.match("^<@(!|)[0-9]+>$", msg):
		targetid = msg.lstrip('<@!').rstrip('>')
		seen_at = database.last_seen_id(targetid)

	else:
		for u in client.c.get_all_members():
			nick = u.nick or u.name
			if msg == nick.lower():
				if str(u.status) == "online":
					online = True
				break

		seen_at = database.last_seen(msg)

	if online and seen_at:
		client.reply(channel, user, "He is right there, dumbass. Last log off {0} ago.".format(str(datetime.timedelta(seconds=int(time.time())-seen_at))))
	elif online and not seen_at:
		client.reply(channel, user, "He is right there, dumbass. No log off records found for *{0}*.".format(msg))
	elif seen_at:
		client.reply(channel, user, "Seen **{0}** logging off {1} ago.".format(msg, str(datetime.timedelta(seconds=int(time.time())-seen_at))))
	else:
		client.reply(channel, user, "No records found for *{0}*.".format(msg))

def get_nicks(channel, user, msg):
	if not len(msg):
		client.reply(channel, user, "You must specify a nick name.")
		return

	nicks = database.get_nicks(msg)
	if nicks:
		client.notice(channel, "**{0}** was also known as {1}".format(msg, ", ".join(["`{0}`".format(i) for i in nicks])))
	else:
		client.notice(channel, "No records found for *{0}*.".format(msg))

def get_emoji_list(channel, user):
	console.display("count: {0}".format(len(client.c.guilds)))
	for server in client.c.guilds:
		console.display(server.name)
		if len(server.emojis):
			client.private_reply(user, "**"+server.name+":**")
			n = 8
			my_list = ["<:{0}:{1}> {2}".format(emoji.name, emoji.id, emoji.name) for emoji in server.emojis]
			for i in [my_list[i * n:(i + 1) * n] for i in range((len(my_list) + n - 1) // n )]:
				client.private_reply(user, " | ".join(i))

def get_emoji(channel, user, args, gif=False):
	if not len(args):
		client.reply(channel, user, "You must specify an emoji.")

	#define if its an emoji or text
	match = emoji_re.match(args[0])
	if match:
		target = match.group(1)
	else:
		target = args[0]

	for server in client.c.guilds:
		for emoji in server.emojis:
			#print([emoji.name, emoji.id, emoji.url, str(emoji)])
			if emoji.name.lower() == target:
				embed = Embed()
				if gif:
					embed.set_image(url=re.sub("\.png$", ".gif", emoji.url))
				else:
					embed.set_image(url=emoji.url)
				client.notice_embed(channel, embed)
				return

	client.reply(channel, user, "Emoji *{0}* not found on known servers.".format(args[0]))

def post_anime(channel, user, args):
	if not len(args):
		tags = 'solo+1girl'
	else:
		tags = "+".join(args)

	if channel.guild.id == 135337712210149376: #pomru
		if channel.id == 222756372851195904: #isolator
			url = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags=rating:safe+"+tags
		else:
			client.reply(channel, user, ":peka5:")
	else:
		url = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&tags=rating:safe+"+tags

	response = requests.get(url)
	xml = etree.fromstring(response.content)
	if len(xml) == 0:
		client.reply(channel, user, "No anime images found with specified tags!")
		return

	#get random page if we get a lot of posts
	page_count = int(xml.get('count')) // 100
	print(page_count)
	if page_count > 0:
		#limitation of gelbooru API
		if page_count > 200: page_count = 200

		random_page = randint(0, page_count)
		if random_page != 0:
			response.close()
			response = requests.get(url+'&pid='+str(random_page))
			#print(response.content)
			xml = etree.fromstring(response.content)

	response.close()
	#get random image
	num = randint(0, len(xml)-1)
	file_url = xml[num].get('file_url')
	file_id = xml[num].get('id')

	#get tags
	tags = []
	response = requests.get("https://gelbooru.com//index.php?page=post&s=view&id="+file_id)
	x = re.escape('"><a href="index.php?page=wiki&amp;s=list&amp;search=')
	for tag_type in ['artist', 'copyright', 'character']:
		match = re.search('tag-type-'+tag_type+x+'([^"]+)', str(response.content))
		if match:
			tags.append('**{0}**: `{1}` '.format(tag_type, urllib.parse.unquote(match.groups()[0])))
	response.close()
	if len(tags):
		description = ' | '.join(tags)
	else:
		description = None

	embed = Embed(title=file_id, url="https://gelbooru.com//index.php?page=post&s=view&id="+file_id, description = description)
	embed.set_image(url=file_url)
	client.notice_embed(channel, embed)

def get_avatar(channel, user, args):
	if not len(args):
		client.reply(channel, user, "You must specify a user.")

	#search for id
	if re.match("^<@(!|)[0-9]+>$", args[0]):
		target = client.get_member_by_id(channel, args[0])
	#search for nick
	else:
		target = client.get_member_by_nick(channel, " ".join(args))

	if target:
			embed = Embed()
			embed.set_image(url=target.avatar_url)
			client.notice_embed(channel, embed)
	else:
		client.reply(channel, user, "Could not find this user on the server.")

def gizma_quote(channel, user, msg):
	msg = "\r\n".join(split_large_message(msg, delimiter=" ", charlimit=27))
	embed = Embed()
	embed.add_field(name='Gizma', value=msg)
	embed.set_image(url=gizma_url)
	client.notice_embed(channel, embed)

def on_member_join(member):
	if member.guild.id == 135337712210149376: #promode.ru  135337712210149376
		chan = client.c.get_channel(135337712210149376) ##general

		if database.check_greetings(member.id, member.guild.id): #if allready have been welcomed before
			client.reply(chan, member, "добро пожаловать. Снова. <:peka9:388824495147712512>")
		else:
			database.add_greeting(member.id, member.guild.id)
			client.reply(chan, member, "добро пожаловать в русcкоязычное <:cpm2:418827428694065162>**промод** сообщество! Просьба ознакомиться с разделами секции 🡪 INFO, в них ты найдешь ссылку на игру, гайды, правила, узнаешь как поиграть и т.д. Если ты новичок, **сообщи об этом модератору!**<:flagr:332563175373275137>")

def split_large_message(text, delimiter="\n", charlimit=1999):
	templist = text.split(delimiter)
	tempstr = ""
	result = []
	for i in range(0,len(templist)):
		tempstr += templist[i]
		msglen = len(tempstr)
		if msglen >= charlimit:
			raise("Text split failed!")
		elif i+1 < len(templist):
			tempstr += delimiter
			if msglen+len(templist[i+1]) >= charlimit-2:
				result.append(tempstr)
				tempstr = ""
		else:
			result.append(tempstr)
	return result
