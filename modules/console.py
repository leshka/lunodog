#!/usr/bin/python3
# encoding: utf-8

from threading import Thread
from multiprocessing import Queue
import sys, os, datetime, readline, rlcompleter

from modules import config, bot#, stats

def init():
	global thread, log, userinput_queue, alive

	alive = True
	#init log file
	if not os.path.exists(os.path.abspath("logs")):
	  os.makedirs('logs')
	log = open(datetime.datetime.now().strftime("logs/log_%Y-%m-%d-%H:%M"),'w')
	
	userinput_queue = Queue()

	#init user console
	thread = Thread(target = userinput, name = "Userinput")
	thread.daemon = True
	thread.start()

		
def userinput():
	readline.parse_and_bind("tab: complete")
	while 1:
		inputcmd=input()
		userinput_queue.put(inputcmd)

def run():
	try:
		cmd = userinput_queue.get(False)
		display("CONSOLE| /"+cmd)
		try:
			exec(cmd)
		except Exception as e:
			display("CONSOLE| ERROR: "+str(e))
	except:
		pass
			
def display(data):
	text = str(data).encode(sys.stdout.encoding, 'ignore').decode(sys.stdout.encoding) #have to do this encoding/decoding bullshit because python fails to encode some symbols by default
	text=datetime.datetime.now().strftime("(%H:%M:%S)")+text # add date and time
	linebuffer=readline.get_line_buffer()
	sys.stdout.write("\r\n\033[F\033[K"+text+'\r\n>'+linebuffer)
	log.write(text+'\r\n')

def terminate():
	global alive
#	stats.close()
	print("Waiting for connection to close...")
	alive = False
