#!/usr/bin/python3
# encoding: utf-8

from modules import console
from importlib.machinery import SourceFileLoader

def init(dirname=""):
	global cfg
	
	#load config
	try:
		cfg = SourceFileLoader('cfg', 'config.cfg').load_module()
	except Exception as e:
		console.display("ERROR| ERROR PARSING config.cfg FILE!!! {0}".format(str(e)))
		console.terminate()
